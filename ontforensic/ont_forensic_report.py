__author__ = "Martin Haagmans (https://gitlab.com/MartinHaagmans)"
__license__ = "MIT"

import sys
import yaml
import xlsxwriter
  
MINIONQC = ['total.gigabases', 'total.reads', 'N50.length', 'mean.length', 'median.length', 'max.length', 'mean.q', 'median.q']
MINIONQC_READS = ['>10kb', '>20kb', '>50kb', '>100kb', '>200kb', '>500kb', '>1m', 'ultralong']
VCF_HEADER =['LOCUS', 'RSID', 'REF', 'ALT', 'GENOTYPE', 'DEPTH', 'ALLELE FREQUENCY']
STR_HEADER = ['STR', 'Allele', 'NormalizedCounts']

SNP_RSID = {
    'chr11:88911696': 'rs1042602',
    'chr11:89011046': 'rs1393350',
    'chr11:89017961': 'rs1126809',
    'chr12:89328335': 'rs12821256',
    'chr14:92773663': 'rs12896399',
    'chr14:92801203': 'rs2402130',
    'chr14:92882826': 'rs17128291',
    'chr15:28187772': 'rs1545397',
    'chr15:28197037': 'rs1800414',
    'chr15:28230318': 'rs1800407',
    'chr15:28271775': 'rs12441727',
    'chr15:28288121': 'rs1470608',
    'chr15:28356859': 'rs1129038',
    'chr15:28365618': 'rs12913832',
    'chr15:28453215': 'rs2238289',
    'chr15:28496195': 'rs6497292',
    'chr15:28530182': 'rs1667394',
    'chr15:48426484': 'rs1426654',
    'chr16:89383725': 'rs3114908',
    'chr16:89984378': 'rs3212355',
    'chr16:89985844': 'rs1805005',
    'chr16:89985918': 'rs1805006',
    'chr16:89985940': 'rs2228479',
    'chr16:89986091': 'rs11547464',
    'chr16:89986117': 'rs1805007',
    'chr16:89986130': 'rs1110400',
    'chr16:89986144': 'rs1805008',
    'chr16:89986154': 'rs885479',
    'chr16:89986546': 'rs1805009',
    'chr16:90024206': 'rs8051733',
    'chr20:32665748': 'rs6059655',
    'chr20:32785212': 'rs6119471',
    'chr20:33218090': 'rs2378249',
    'chr5:33951693': 'rs16891982',
    'chr5:33958959': 'rs28777',
    'chr6:396321': 'rs12203592',
    'chr6:457748': 'rs4959270',
    'chr9:1270905': 'rs683',
    'chr9:16858084': 'rs10756819'
}

RSID_REFBASE = {
    'rs1042602': 'C',
     'rs1393350': 'G',
     'rs1126809': 'G',
     'rs12821256': 'T',
     'rs12896399': 'G',
     'rs2402130': 'G',
     'rs17128291': 'A',
     'rs1545397': 'A',
     'rs1800414': 'T',
     'rs1800407': 'C',
     'rs12441727': 'G',
     'rs1470608': 'G',
     'rs1129038': 'C',
     'rs12913832': 'A',
     'rs2238289': 'A',
     'rs6497292': 'A',
     'rs1667394': 'C',
     'rs1426654': 'A',
     'rs3114908': 'T',
     'rs3212355': 'C',
     'rs1805005': 'G',
     'rs1805006': 'C',
     'rs2228479': 'G',
     'rs11547464': 'G',
     'rs1805007': 'C',
     'rs1110400': 'T',
     'rs1805008': 'C',
     'rs885479': 'G',
     'rs1805009': 'G',
     'rs8051733': 'A',
     'rs6059655': 'A',
     'rs6119471': 'C',
     'rs2378249': 'G',
     'rs16891982': 'C',
     'rs28777': 'C',
     'rs12203592': 'C',
     'rs4959270': 'C',
     'rs683': 'C',
     'rs10756819': 'G'
}

COMP_RSID = [
    'rs1042602',  'rs1129038',  'rs12821256',  'rs12913832',  'rs1393350',  'rs1470608',  'rs17128291',  
    'rs1800407',  'rs2238289',  'rs2378249',  'rs3212355',  'rs6059655',  'rs6497292',  'rs683',  
    'rs8051733',  'rs885479'
]

STR_NAMES = [
    'D12S391', 'D13S317', 'D16S539', 'D18S51', 'D19S443', 'D1S1656', 
    'D21S11', 'D22S1045', 'D2S1338', 'D2S441', 'D3S1358', 'D5S818', 
    'D7S820', 'D8S1179', 'FGA', 'PentaD', 'PentaE', 'TH01', 'TPOX', 'vWA'
    ]


def get_complement_base(base):
    if base == 'A':
        return 'T'
    elif base == 'T':
        return 'A'
    elif base == 'G':
        return 'C'
    elif base == 'C':
        return 'G'
    else:
        raise ValueError('Base not recognized as canonical')

def write_list(listname, worksheet, row=0, col=0, skip=1, header=False,
               orientation='rows', format=None, formatheader=None):
    if header:
        worksheet.write(row, col, header, formatheader)
        row += skip

    if orientation == 'rows':
        [worksheet.write(row + i, col, ii, format)
         for i, ii in enumerate(listname)]
        row = row + len(listname) + 2

    elif orientation == 'cols':
        [worksheet.write(row, col + i, ii, format)
         for i, ii in enumerate(listname)]
        col = col + len(listname) + 2
        row += 1

    return (row, col)


def parse_qc_summary(fn_summary):
    with open(fn_summary) as f:
        summary = yaml.load(f, Loader=yaml.FullLoader)

    all_reads = summary['All reads']
    qual_reads = summary['Q>=7']
    
    return (all_reads, qual_reads)


def parse_adaptive_report(fn_report):
    report = dict()
    with open(fn_report) as f:
        for line in f:
            report_value, metric = line.split()
            if metric == 'decision':
                continue
            elif metric == 'no_decision':
                metric = '% no decision'
            elif metric == 'stop_receiving':
                metric = '% sequenced'
            elif metric == 'unblock':
                metric = '% reversed'
            report[metric] = int(report_value)
    return report


def normalize_report(report):
    total = int()
    normalized_report = dict()
    for metric, report_value in report.items():
        total += report_value
    for metric, report_value in report.items():
        normalized_report_value = report_value / total * 100
        normalized_report[metric] = f'{normalized_report_value:.2f}'
    return normalized_report
    
    
def parse_vcf(fn_vcf):
    variants_called = dict()
    with open(fn_vcf) as f:
        for line in f:
            if line.startswith('#'):
                continue
            chrom, pos, id, ref, alt, qual, filter, info, format, call = line.split()
            genotype, genotype_qual, depth, allele_frequency = call.split(':')

            locus = f'{chrom}:{pos}'
            rsid = SNP_RSID[locus]

            if rsid in COMP_RSID:
                ref = get_complement_base(ref)
                alt = get_complement_base(alt)            
            
            if genotype == '0/1':
                genotype = f'{ref}{alt}'
            elif genotype == '1/1':
                genotype = f'{alt}{alt}'
            
            variants_called[locus] = dict()
            variants_called[locus]['ref'] = ref
            variants_called[locus]['alt'] = alt
            variants_called[locus]['genotype'] = genotype
            variants_called[locus]['depth'] = int(depth)
            variants_called[locus]['allele_frequency'] = float(allele_frequency)
    return variants_called


def parse_snp_bed(fn_coverage_snps):
    snp_coverage = dict()
    with open(fn_coverage_snps) as f:
        for line in f:
            chrom, _start, end, depth = line.split()
            locus = f'{chrom}:{end}'
            snp_coverage[locus] = int(depth.split('.')[0])
    return snp_coverage
    
    
def parse_str_top_two_alleles(fn):
    alleles = list()
    with open(fn) as f:
        try:
            header = next(f)
        except StopIteration:
            print(fn)
        for line in f:
            str, allele, normalized_counts = line.split()
            alleles.append((str, allele, normalized_counts))
    return alleles
    
            
            

def create_excel(sample, metrics_adaptive, metrics_run_all_reads, metrics_run_qual_reads, variants_called, snp_coverage, str_calls):
    wb = xlsxwriter.Workbook(f'{sample}.xlsx')
    wb.set_properties({
        'title':    sample,
        'subject':  'ONT Forensic',
        'author':   'Boudicea',
        'comments': 'Created with Python and XlsxWriter'
        })
    
    ws1 = wb.add_worksheet('QC')
    ws2 = wb.add_worksheet('SNP')
    ws3 = wb.add_worksheet('STR')
    
    headerformat = wb.add_format()
    headerformat.set_font_size(16)
    
    underlined = wb.add_format()
    underlined.set_bottom()
    
    gray = wb.add_format()
    gray.set_bg_color('gray')
    
    red = wb.add_format()
    red.set_bg_color('red')
   
    metrics_adaptive_name = sorted(metrics_adaptive.keys())
    metrics_adaptive_value = [metrics_adaptive[name] for name in metrics_adaptive_name]
    
    metrics_run_all_reads_value = (
        [metrics_run_all_reads[name] for name in MINIONQC] +
        [metrics_run_all_reads['reads'][name] for name in MINIONQC_READS]
        )
        
    metrics_run_qual_reads_value = (
        [metrics_run_qual_reads[name] for name in MINIONQC] +
        [metrics_run_qual_reads['reads'][name] for name in MINIONQC_READS]
        )        
    
    
    # First fill in the patient info
    sample_names = ['Sample', 'Age', 'Origin']
    sample_values = [sample, '', '']

    row1, col1 = write_list(sample_names, ws1, header='ONT REPORT CAS+AS', formatheader=headerformat)
    row1, col1 = write_list(sample_values, ws1, row=row1 - len(sample_names)-2, col=1)

    #Adaptive results
    row1, col1 = write_list(metrics_adaptive_name, ws1, header='ADAPTIVE RESULT', row=row1-1, formatheader=headerformat)
    row1, col1 = write_list(metrics_adaptive_value, ws1, row=row1 - len(metrics_adaptive_name) - 2, col=1)

    #Run report
    row1, col1 = write_list(MINIONQC + MINIONQC_READS, ws1, header='RUN RESULT', row=row1-1, formatheader=headerformat)
    row1, col1 = write_list(metrics_run_all_reads_value, ws1, row=row1 - len(MINIONQC + MINIONQC_READS) - 2, col=1)                            
    row1, col1 = write_list(metrics_run_qual_reads_value, ws1, row=row1 - len(MINIONQC + MINIONQC_READS) - 2, col=2) 
    
    
    row2, col2 = write_list(VCF_HEADER, ws2, format=underlined, header='ONT SNP CALLING', formatheader=headerformat, orientation='cols')
    
    for locus in variants_called.keys():
        rsid = SNP_RSID[locus]
        ref = variants_called[locus]['ref']
        alt = variants_called[locus]['alt']
        genotype = variants_called[locus]['genotype']
        depth = variants_called[locus]['depth']
        allele_frequency = variants_called[locus]['allele_frequency']
        
        vcf_output = [locus, rsid, ref, alt, genotype, depth, allele_frequency]
   
        row2, col2 = write_list(vcf_output, ws2, orientation='cols', row=row2)
        
    for locus in snp_coverage.keys():
        if locus in variants_called:
            continue
        rsid = SNP_RSID[locus]
        ref = RSID_REFBASE[rsid]
        alt = str()
        genotype = f'{ref}{ref}'
        depth = snp_coverage[locus]
        allele_frequency = str()

        bed_output = [locus, rsid, ref, alt, genotype, depth, allele_frequency]
   
        row2, col2 = write_list(bed_output, ws2, orientation='cols', row=row2)        
        
    row3, col3 = write_list(STR_HEADER, ws3, header='ONT STR CALLING', format=underlined, formatheader=headerformat, orientation='cols')
    
    for str_call in str_calls:
        row3, col3 = write_list(str_call, ws3, orientation='cols', row=row3)
    
    wb.close()
    return


def main(sample, fn_metrics_run, fn_metrics_adaptive, fn_vcf, fn_top_two_alleles):
    all_reads, qual_reads = parse_qc_summary(fn_metrics_run)
    
    
    _metrics_adaptive = parse_adaptive_report(fn_metrics_adaptive)
    metrics_adaptive = normalize_report(_metrics_adaptive)
    
    
    variants_called = parse_vcf(fn_vcf)
    snp_coverage = parse_snp_bed(fn_coverage_snps)
    
    str_calls = list()
    
    for fn in fn_top_two_alleles:
        str_calls += parse_str_top_two_alleles(fn)

    
    create_excel(sample, metrics_adaptive, all_reads, qual_reads, variants_called, snp_coverage, str_calls)
    


    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Parse stuff for report")
    parser.add_argument('--folder', type=str, required=True,
                        help='Input folder')                            
    parser.add_argument('--sample', type=str, required=True,
                        help='SampleID as in folder')
    args = parser.parse_args()
    sample = args.sample
    input_dir = args.folder
    
    #Define files
    fn_metrics_run = f'{input_dir}/{sample}.sum.txt'
    fn_metrics_adaptive = f'{input_dir}/{sample}.counts.txt'
    fn_vcf = f'{input_dir}/clair3/merge_output.vcf'
    fn_coverage_targets = f'{input_dir}/mosdepth/{sample}_mosdepth.regions.bed'
    fn_coverage_snps = f'{input_dir}/mosdepth/{sample}_SNP_mosdepth.regions.bed'
    
    fn_top_two_alleles = [f'{input_dir}/ParallelOutput_dir/Countings/{STR}_{sample}.sorted.bam_Toptwo.txt' for STR in STR_NAMES]
    
    
    main(sample, fn_metrics_run, fn_metrics_adaptive, fn_vcf, fn_top_two_alleles)
