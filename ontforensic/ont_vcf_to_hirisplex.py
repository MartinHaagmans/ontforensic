#!/usr/bin/env python
__author__ = "Martin Haagmans (https://gitlab.com/MartinHaagmans)"
__license__ = "MIT"

import csv

from static_input import LOCUS_RSID
from static_input import RSID_REFBASE
from static_input import HIRISPLEXS
from static_input import REF_IS_INPUT
from static_input import REFCOMP_IS_INPUT
from static_input import ALT_IS_INPUT
from static_input import ALTCOMP_IS_INPUT
from static_input import IMPOSSIBLE
from static_input import HIRISPLEXS_HEADER

RSIDS = RSID_REFBASE.keys()

def get_complement_base(base):
    "Input single base, return complement base"
    if base == 'A':
        return 'T'
    elif base == 'T':
        return 'A'
    elif base == 'G':
        return 'C'
    elif base == 'C':
        return 'G'
    else:
        raise ValueError('Base not recognized as canonical')


def parse_vcf_to_genotypes(vcf_fn, coverage, cutoff=2):
    "Parse vcf to ref/alt genotypes. Return ref/ref if rsid not in vcf"
    genotypes = dict()

    with open(vcf_fn) as f:
        for line in f:
            if line.startswith('#'):
                continue

            chrom, pos, _id, ref, alt, _qual, _filter, _info, _format, sample_call = line.split()
            zygosity = sample_call.split(':')[0]

            locus = f'{chrom}:{pos}'
            rsid = LOCUS_RSID[locus]

            if zygosity == '0/1':
                genotype = f'{ref}/{alt}'
            elif zygosity == '1/1':
                genotype = f'{alt}/{alt}'

            genotypes[rsid] = genotype

    for rsid in RSIDS:
        if not rsid in genotypes:
            refbase = RSID_REFBASE[rsid]
            if coverage[locus] >= cutoff:
                genotypes[rsid] = f'{refbase}/{refbase}'     
            elif coverage[locus] < cutoff:
                genotypes[rsid] = 'NA'     
    return genotypes


def convert_genotypes_to_hirisplex_calls(genotypes):
    hirisplexs_calls = dict()
    
    for rsid in HIRISPLEXS:
    
        assert rsid in genotypes
        assert not rsid in hirisplexs_calls
        
        refbase = RSID_REFBASE[rsid]
        
        if rsid in REF_IS_INPUT or rsid in REFCOMP_IS_INPUT:
            nr_alleles = genotypes[rsid].count(refbase)
        elif rsid in ALT_IS_INPUT:
            input_base = HIRISPLEXS[rsid]
            nr_alleles = genotypes[rsid].count(input_base)
        elif rsid in ALTCOMP_IS_INPUT:
            input_base = get_complement_base(HIRISPLEXS[rsid])
            nr_alleles = genotypes[rsid].count(input_base)
        elif genotypes[rsid] == 'NA':
            nr_alleles = 'NA'
        elif rsid in IMPOSSIBLE:
            nr_alleles = 'NA'
            

        hirisplexs_calls[rsid] = nr_alleles

    return hirisplexs_calls
     
     
def parse_bed_for_SNP_coverage(bed_fn):
    "Parse bed to get coverage for RS positions"
    coverage = dict()
    with open(bed_fn) as f:
        for line in f:
            chrom, start, end, cov = line.split()
            start = int(start)
            end = int(end)
            while start <= end:
                locus = f'{chrom}:{start}'
                coverage[locus] = int(float(cov))
                start += 1
    return coverage
    

def create_output(hirisplexs_calls, sample):
    calls = list()
    for rsid_base in HIRISPLEXS_HEADER:
        rsid, base = rsid_base.split('_')
        calls.append(str(hirisplexs_calls[rsid]))

    header = 'sampleID,' + ','.join(HIRISPLEXS_HEADER)
    calls_output = f'{sample},' + ','.join(calls)

    with open(f'{sample}_hirisplexs.csv', 'w') as f:
        f.write(header)
        f.write('\n')
        f.write(calls_output)
        f.write('\n')
    return 


def parse_hirisplex_output(hirisplex_fn):
    sample = get_sample(hirisplex_fn)
    with open(hirisplex_fn) as f, open(f'{sample}_percentages.txt', 'w') as f_out:
        reader = csv.reader(f)
        header = next(reader)
        for line in reader:
            for i, call in enumerate(line):
                if header[i].startswith('rs'):
                    continue
                f_out.write(f'{header[i]}\t{call}\n') 
                
    return

def get_sample(fn):
    sample = vcf_fn.split('.')[0]
    return sample
    

def main(vcf_fn, bed_fn):
    sample = get_sample(vcf_fn)
    coverage = parse_bed_for_SNP_coverage(bed_fn)
    calls = parse_vcf_to_genotypes(vcf_fn, coverage)
    
    print(coverage)
    hirisplex_calls = convert_genotypes_to_hirisplex_calls(calls)
    create_output(hirisplex_calls, sample)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(
        description="Parse VCF and BED to create input for https://hirisplex.erasmusmc.nl/"
        )
    parser.add_argument('--input', type=str, required=True, help='VCF file or Hirisplex csv')
    parser.add_argument('--bed', type=str, required=True, help='mosdepth BED with coverage for SNP positions')                        
    args = parser.parse_args()
    
    if args.input.endswith('vcf'):
        vcf_fn = args.input
        bed_fn = args.bed
        main(vcf_fn, bed_fn)
    elif args.input.endswith('csv'):
        parse_hirisplex_output(args.input)