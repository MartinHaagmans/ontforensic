__author__ = "Martin Haagmans (https://gitlab.com/MartinHaagmans)"
__license__ = "MIT"

import pysam
from statistics import mean, stdev

YSTRS = [
    'DYF387S1',
    'DYS19',
    'DYS385a-b',
    'DYS389I',
    'DYS389II',
    'DYS390',
    'DYS391',
    'DYS392',
    'DYS437',
    'DYS438',
    'DYS439',
    'DYS448',
    # 'DYS460', #Niet in MiSeq set
    'DYS481',
    # 'DYS505', #Niet in MiSeq set
    # 'DYS522', #Niet in MiSeq set
    'DYS533',
    'DYS549',
    'DYS570',
    'DYS576',
    # 'DYS612', #Niet in MiSeq set
    'DYS643',
    # 'DYS653', #Niet in MiSeq set
    'Y-GATA-H4'
    ]


def get_alignment_length_and_score_from_read(read):
    ref = read.reference_name
    cigar = read.cigartuples
    tags = read.get_tags()
    alignment_length = int()
    for code, length in cigar:
        if code in [0, 1, 2,]:
            alignment_length += length
    for tag_name, tag_value in tags:
        if tag_name == 'AS' :
            alignment_score = tag_value
    return ref, alignment_length, alignment_score


def get_data_from_bam(bam_fn):
    data = dict()
    bam = pysam.AlignmentFile(bam_fn, 'rb')
    
    for read in bam:
        if read.is_mapped:
            ref, alignment_length, alignment_score = get_alignment_length_and_score_from_read(read)
        elif not read.is_mapped:
            continue
        else:
            raise ValueError('Read should be mapped or not')

        if ref in data:
            data[ref].append((alignment_length, alignment_score))
        elif not ref in data:
             data[ref] = list()
             data[ref].append((alignment_length, alignment_score))
             
    return data


def get_alignment_data_for_refs(data):
    data_out = list()

    for ref in sorted(data.keys()):
        alignments = len(data[ref])
        alignment_lengths = list()
        alignment_scores = list()
        for alen, ascore in data[ref]:
            if int(ascore) <= 0.9 * int(alen):
                continue

            alignment_lengths.append(alen)
            alignment_scores.append(ascore)
        if len(alignment_lengths) <= 1:
            continue
        mean_alignment_length  = mean(alignment_lengths)
        mean_alignment_score = mean(alignment_scores) 
        stdev_alignment_length  = stdev(alignment_lengths)
        stdev_alignment_score = stdev(alignment_scores)

        data_out.append((
            ref,  alignments, 
            int(mean_alignment_length), int(stdev_alignment_length),  
            int(mean_alignment_score), int(stdev_alignment_score)
            ))
    return data_out


def get_highest_scores_per_STR(data):
    data_out = dict()
    for str_name in YSTRS:
        highest_mean_alignment_length = 0
        highest_mean_alignment_score = 0
        data_out[str_name] = dict()
        data_out[str_name]['highest_mean_alignment_length'] = str()
        data_out[str_name]['highest_mean_alignment_score'] = str()
        for ref, alignments, mean_length, std_length, mean_score, std_score in data:
            if ref.startswith(f'{str_name}.'):
                if mean_length > highest_mean_alignment_length:
                    highest_mean_alignment_length = mean_length
                    data_out[str_name]['highest_mean_alignment_length'] = ref
                elif mean_length == highest_mean_alignment_length:
                    print(f'{ref} has same highest_mean_alignment_length as other ref')                    
                if mean_score > highest_mean_alignment_score:
                    highest_mean_alignment_score = mean_score
                    data_out[str_name]['highest_mean_alignment_score'] = ref
                elif mean_score == highest_mean_alignment_score:
                    print(f'{ref} has same highest_mean_alignment_score as other ref')
                    
    return data_out
    

def write_calls_to_file(data, sample):
    with open(f'{sample}_Y-STR.txt', 'w') as f:
        header = 'REF\tCall\n'
        f.write(header)
        for str_name in data:
            # most_alignments = data[str_name]['most_alignments']
            highest_mean_alignment_length = data[str_name]['highest_mean_alignment_length']
            highest_mean_alignment_score = data[str_name]['highest_mean_alignment_score']
            if not highest_mean_alignment_length == highest_mean_alignment_score:
                print(str_name)
            nr_of_str_repeats = highest_mean_alignment_score.replace(f'{str_name}.', '')
            f.write(f'{str_name}\t{nr_of_str_repeats}\n')
    return
    
    
def main(bam_fn, sample):
    mapped_reads = get_data_from_bam(bam_fn)
    alignment_data = get_alignment_data_for_refs(mapped_reads)
    scores = get_highest_scores_per_STR(alignment_data)
    write_calls_to_file(scores, sample)


if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(description="Parse BAM to get Y-STR calls")
    parser.add_argument('--bam', type=str, required=True,
                        help='BAM file to parse')                            
    parser.add_argument('--sample', type=str, required=True,
                        help='SampleID as in folder')
    args = parser.parse_args()
    sample = args.sample
    bam_fn = args.bam
    
    main(bam_fn, sample)
    
